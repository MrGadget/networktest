# NetworkTest

## Introduction

This project named poorly "NetworkTest" is supposed to be a Unity example project based on the Mirror implementation of the HLAPI of Unity 3D Networking.
I aimed to make a comprehensive, simple yet complete project which can be used as blueprint for a TPS/FPS multiplayer game.

## The "Game"

The game is a simple TPS.
The player that host the game is not a real player. It's just a camera that show the list of connected players.
The players that are classical clients.

So host and players have different prefabs that are instantiated by a different process.

Their is also a "Host as player" mod but it's not ready yet and I can't vouch for it. 

When a player connect to the server, it's asked to select a team (red of blue) which makes it spawn in the right location.
A player can move and shot in front of it. If its bullet touch another player, this player lost HPs. 
If a player reach 0 HP, it's dead and have to respawn (again, on the first screen).
When a player hit the "Tab" button, it can see the list of player.
The "K" key kill the player (to test the respawn function).

### About the achitecture

Why do I use a special prefab and system for the host of the game?
Because I want to be able to create a "Server only" executable, that can be hosted in a virtual machine.
In the case of a casual play, it's not really needed, but if you want to have a competitive/ranking system in your game, 
it can become handy, as having a server hosted on a controlled server can help you fight hacking and cheating in your game.

## Current status
The current status of the project is NOT WORKING AT ALL.

### Unity version and requirements
The project is based on Unity 2019.2.5f1 and the project use TextMeshPro.

## Other documentation
Check the Mirror [Documentation](https://mirror-networking.com/docs/) for more information on the "API" used.

## Contributor

- Me (Captain Nakou)

### Special Thanks

- Rewar (For helping trying to clean this mess)
- The A-Team (Aelitz, Leinox, Sighne, Baenshee, for help and emotionnal support)

## Current Issues

I am working on the project, so issues can change along with my modifications of the code, but most of the time, the issues are :

Most frequent issues are :
- HUD becoming non clickable for the second player.
- When the player is connected but dead (and is not showing on the client), it's still visible for the others (and for the server).
- The bullets sometime doesn't register the hit on another player (or pass through walls).
- The bullets of Player 1 doesn't register hit on Player 2.

Some other issues can randomly appear with modifications in the code :
- If you connect a second player, the first one will use the camera of the second one (but still controlling the first one).
- If you connect to the server, you'll spawn with a prefab and the interface as an "alive" player, but you wont be able to move nor shoot, your team will be the "Observer team" and your HPs will be 0.
- States "Alive" and "Dead" actives at the same time (in term components/gameobjects).
- If you connect to the server, the server will have your name register in the SyncVar "pseudo" of your prefab, but the name on the player will be showed wrongly as "Pseudo".
- And more...

## TODO

- Make it work.
- Show Player HP on HUD.
- Show Player HP above other players.
- Show the kills in the top-right of the screen.
- Make a better score screen for players.
- Make a cool menu with a "Disconnect" button.
- Make a cool main menu to create/connect to games without the old ugly Unity Network HUD.

## Licence

This project is under MIT licence.