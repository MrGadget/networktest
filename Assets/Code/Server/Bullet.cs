﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Bullet : NetworkBehaviour
{
    public float destroyAfter = 5;
    public float ejectionForce = 5000;

    public PlayerInformations origin;
    public Vector3 forceEjectionDirection;

    public new Rigidbody rigidbody;

    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), destroyAfter);
    }

    void Start()
    {
        rigidbody.AddForce(forceEjectionDirection * ejectionForce);
    }

    // destroy for everyone on the server
    [Server]
    void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }

    // ServerCallback because we don't want a warning if OnTriggerEnter is called on the client
    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Touchy : " + other + ")");
        PlayerInformations informations = other.GetComponentInParent<PlayerInformations>();
        if (informations != null)
        {
            RpcOnTouchy("Damage from [" + origin.pseudo + "] on [" + informations.pseudo +"]");
            informations.TakeDamages(5);
        }
        Debug.Log("Destroy me! (Shot by : " + origin.pseudo + ")");
        NetworkServer.Destroy(gameObject);
    }

    [ClientRpc]
    void RpcOnTouchy(string message)
    {
        Debug.Log(message);
    }
}
