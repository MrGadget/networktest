﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

/**
 * The Server Player is a special "Host" player that spawn instead of a normal player prefab when you start the game in "Host" mode. 
 * The idea behind is to have a Server-only player that can't play.
 * It's just a security mesure : in UNET, the server is a god that has access to everything. Having a player with that power is not good if you want
 * your game to have a competitive mod (hacking).
 * 
 * You can imagine a situation when the server.exe is deployed on a gamedev controlled server only and player connect themselves on it.
 **/
public class ServerPlayer : NetworkBehaviour
{
    public GameObject cam;
    public TextMeshProUGUI listPlayerRed;
    public TextMeshProUGUI listPlayerBlue;
    public TextMeshProUGUI listPlayerAll;

    public override void OnStartServer()
    {
        cam.SetActive(true);
    }

    void Update()
    {
        //listPlayerBlue.text = SuperGameManager.singleton.printBlueList();
        //listPlayerRed.text = SuperGameManager.singleton.printRedList();
        //listPlayerAll.text = SuperGameManager.singleton.printPlayerList();
    }
}
