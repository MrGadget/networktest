﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

/**
 * This object is normally just a singleton that manage a syncList of the connected Players.
 * So people can access it to find list of other player connected.
 * 
 * I added a log system too for debbuging purposes.
 * 
 * You can also imagine a chat system would be handled in the same way.
 **/
public class SuperGameManager : NetworkBehaviour
{
    //SyncList<PlayerInformations> playerList = new SyncListItem();
    //class SyncListItem : SyncList<PlayerInformations> { }

    public static SuperGameManager singleton;

    [SyncVar(hook = nameof(OnLogAdded))] public string logs;
    public TextMeshProUGUI logsUI;

    public void OnLogAdded(string value)
    {
        logsUI.text = logs;
    }


    public void Awake()
    {
        InitializeSingleton();
        logs = "Logs : ";
    }


    void InitializeSingleton()
    {
        Debug.Log("Initialize Singleton");
        if (singleton != null && singleton == this)
        {
            return;
        }
        singleton = this;
    }

    public void AddSharedLog(string message)
    { 
        logs += "\n" + message;
    }

    //public void AddPlayer(GameObject player)
    //{
    //    playerList.Add(player.GetComponent<PlayerInformations>());
    //}

    //public void RemovePlayer(GameObject player)
    //{
    //    playerList.Remove(player.GetComponent<PlayerInformations>());
    //}

    //public string printPlayerList()
    //{
    //    string list = "";
    //    foreach(PlayerInformations pC in playerList)
    //    {
    //        list += "\n " + pC.pseudo;
    //    }
    //    return list;
    //}

    //public string printRedList()
    //{
    //    string list = "";
    //    foreach (PlayerInformations pC in playerList)
    //    {
    //        if(pC.team == Team.RED)
    //        {
    //            list += "\n " + pC.pseudo;
    //        }
    //    }
    //    return list;
    //}

    //public string printBlueList()
    //{
    //    string list = "";
    //    foreach (PlayerInformations pC in playerList)
    //    {
    //        if (pC.team == Team.BLUE)
    //        {
    //            list += "\n " + pC.pseudo;
    //        }
    //    }
    //    return list;
    //}
}
