﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine;
using Mirror;

public class SuperNetworkManager : NetworkManager
{
    public Transform blueTeamSpawnPosition;
    public Transform redTeamSpawnPosition;
    public GameObject serverPrefab;
    public bool isServer;

    public override void OnServerAddPlayer(NetworkConnection conn, AddPlayerMessage extraMessage)
    {
        GameObject player;
        Transform startPos = GetStartPosition();

        if (isServer)
        {
            player = startPos != null
                ? Instantiate(serverPrefab, startPos.position, startPos.rotation)
                : Instantiate(serverPrefab);
            isServer = false;
        }
        else
        {
            player = startPos != null
                ? Instantiate(playerPrefab, startPos.position, startPos.rotation)
                : Instantiate(playerPrefab);
        }

        NetworkServer.AddPlayerForConnection(conn, player);
    }

}
