﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class PlayerController : NetworkBehaviour
{
    float horizontal = 0;
    float vertical = 0;
    float headRotation = 0;
    bool fire = false;
    public float moveSpeed = 10;

    [Header("Components")]
    public PlayerHUDController playerHUDController;
    public PlayerInformations playerInformations;
    public PlayerVisualsController playerVisualsController;

    [Header("Sub-Components")]
    public GameObject bullet;
    public Transform barrel;
    public Transform head;

    /**
       * This is called when the object is instantiated in the scene "localy".
       **/
    void Start()
    {
        Debug.Log("StartLocal");
    }

    public override void OnStartLocalPlayer()
    {
        Debug.Log("OnStartLocalPlayer");
    }

    /**
     * This is called when the object is instantiated in the scene of the server.
     **/
    public override void OnStartServer()
    {
        Debug.Log("OnStartServer");
        //SuperGameManager.singleton.AddPlayer(gameObject);
    }

    /**
     * This is called when the object is destroyed, so the server can delete the player from the list of connected players.
     **/
    private void OnDestroy()
    {
        Debug.LogWarning("DO ME OnDestroy");
        //SuperGameManager.singleton.RemovePlayer(gameObject);
    }

    /**
     * This is called when the object is destroyed, so the server can delete the player from the list of connected players.
     * Same as OnDestroy, but called at a different time, just in case.
     **/
    public override void OnStopAuthority()
    {
        Debug.LogWarning("DO ME OnStopAuthority");
        //SuperGameManager.singleton.RemovePlayer(gameObject);
    }

    /**
     * Classic Unity Update Loop
     **/
    void Update()
    {
        if (isLocalPlayer && playerInformations.alive)
            Controls();
    }

    /**
     * Classic Unity FixedUpdate Loop
     **/
    void FixedUpdate()
    {
        if (isLocalPlayer && playerInformations.alive)
        {
            transform.Translate(new Vector3(0, 0, vertical * moveSpeed));
            transform.Translate(new Vector3(horizontal * moveSpeed, 0, 0));
            head.transform.Rotate(new Vector3(0, headRotation * moveSpeed, 0));

            if (fire)
                CmdShoot();
        }
    }

    /**
     * This is called when a player select the "Spawn Red" option in the HUD.
     **/
    public void SpawnRed()
    {
        CmdSpawnTeam(Team.RED);
    }

    /**
     * This is called when a player select the "Spawn Blue" option in the HUD.
     **/
    public void SpawnBlue()
    {
        CmdSpawnTeam(Team.BLUE);
    }

    public void spawnPlayer(Team teamSelected)
    {
        playerInformations.SetInformationAtSpawn(teamSelected, 10);
        Transform spawnPosition = null;

        if (teamSelected == Team.RED)
            spawnPosition = NetworkManager.singleton.GetComponent<SuperNetworkManager>().redTeamSpawnPosition;

        if (teamSelected == Team.BLUE)
            spawnPosition = NetworkManager.singleton.GetComponent<SuperNetworkManager>().blueTeamSpawnPosition;

        if (spawnPosition == null)
        {
            Debug.LogError("Spawn position null : issue with determining team to spawn in!");
            playerInformations.SetInformationAtSpawn(Team.OBSERVER, -1);
            return;
        }

        transform.position = spawnPosition.position;
    }

    public void Suicide()
    {
        CmdSuicide();
    }

    // ======================================
    // ------------- COMMANDS ---------------
    // ======================================

    /**
     * Send the instruction to the server to kill the player.
     **/
    [Command]
    public void CmdSuicide()
    {
        SuperLogger("Player [" + playerInformations.pseudo + "] has commited suicide");
        playerInformations.TakeDamages(100);
    }

    [Command]
    void CmdShoot()
    {
        GameObject b = Instantiate(bullet, barrel.position, barrel.rotation);
        b.GetComponent<Bullet>().origin = playerInformations;
        b.GetComponent<Bullet>().forceEjectionDirection = barrel.forward;
        NetworkServer.Spawn(b);
        RpcOnFire();
    }

    /**
     * Send the instruction to the server to spawn the player with health, good team information and spawn position.
     **/
    [Command]
    public void CmdSpawnTeam(Team teamSelected)
    {
        playerInformations.alive = true;
        RpcSpawnTeam(teamSelected);
    }

    // ======================================
    // --------------- RPCS -----------------
    // ======================================

    [ClientRpc]
    public void RpcSpawnTeam(Team teamSelected)
    {
        spawnPlayer(teamSelected);
    }

    [ClientRpc]
    void RpcOnFire()
    {
        SuperLogger("Shot fired by " + playerInformations.pseudo + "!");
    }

    // ======================================
    // --------- CONTROLS AND STUFF ---------
    // ======================================

    void Controls()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        headRotation = Input.GetAxis("Mouse X");
        fire = Input.GetButtonDown("Fire1");
        playerHUDController.showPlayerList(Input.GetButton("Playerlist"));

        if (Input.GetButtonDown("Suicide"))
            CmdSuicide();
    }

    public void SuperLogger(string logContent)
    {
        SuperGameManager.singleton.AddSharedLog(logContent);
        Debug.Log(logContent);
    }
}
