﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class PlayerInformations : NetworkBehaviour
{
    [Header("PlayerVars")]
    [SyncVar] public Team team = Team.OBSERVER;
    [SyncVar(hook = nameof(OnPseudoChanged))] public string pseudo = "Client ";
    [SyncVar] public int health = 0;
    [SyncVar(hook = nameof(OnStatusChanged))] public bool alive;

    [Header("Components")]
    public PlayerHUDController playerHUDController;
    public PlayerController playerController;
    public PlayerVisualsController playerVisualsController;

    public override void OnStartServer()
    {
        base.OnStartServer();

        pseudo = "Player " + Random.Range(1, 1000);
        name = pseudo;
    }

    public void SetInformationAtSpawn(Team team, int health)
    {
        this.team = team;
        this.health = health;
    }

    [Server]
    public void TakeDamages(int value)
    {
        health -= value;

        if (health <= 0)
        {
            health = 0;
            alive = false;
        }

        RpcOnTakingDamage(pseudo, value);
    }

    public void OnPseudoChanged(string newPseudo)
    {
        playerHUDController.setNamePlayer(newPseudo);
        name = newPseudo;
    }

    public void OnStatusChanged(bool isAlive)
    {
        playerController.SuperLogger("Player [" + pseudo + "] is now " + (isAlive ? "ALIVE" : "DEAD"));
        Debug.Log("[isLocalPlayer]=" + isLocalPlayer + " |");

        playerVisualsController.playerBody.SetActive(isAlive);

        if (isLocalPlayer)
        {
            playerHUDController.alivePanel.SetActive(isAlive);
            playerVisualsController.aliveCam.SetActive(isAlive);

            playerHUDController.deathPanel.SetActive(!isAlive);
            playerVisualsController.deathCam.SetActive(!isAlive);
        }
    }

    [ClientRpc]
    void RpcOnTakingDamage(string pseudo, int value)
    {
        playerController.SuperLogger("Ouch! " + pseudo + " took " + value + " damages!");
    }
}
