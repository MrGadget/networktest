﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerVisualsController : NetworkBehaviour
{
    public GameObject playerBody;
    public GameObject aliveCam;
    public PlayerHUDController playerHUDController;

    public AudioListener audioListener;
    public new Rigidbody rigidbody;
    public GameObject deathCam;

    public override void OnStartLocalPlayer()
    {
        audioListener.enabled = true;
        rigidbody.isKinematic = false;
        deathCam.SetActive(true);
    }
}
