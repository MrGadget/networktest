﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;

public class PlayerHUDController : NetworkBehaviour
{
    public GameObject playerCanvas;

    public TextMeshProUGUI playerList;
    public TextMeshProUGUI playerName;
    public GameObject deathPanel;
    public GameObject alivePanel;

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        deathPanel.SetActive(true);
    }

    public void showPlayerList(bool show)
    {
        //if(show)
        //    playerList.text = "Player List : \n" + SuperGameManager.singleton.printPlayerList();
        playerList.gameObject.SetActive(show);
    }

    public void setNamePlayer(string playerName)
    {
        this.playerName.text = playerName;
    }
}
